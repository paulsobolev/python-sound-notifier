# coding=utf-8
from __future__ import unicode_literals
import configparser
import json
import logging
import logging.handlers
import os
import random
import string
import subprocess
import threading
import time
import wave
import uuid

from collections import deque
from lib import websocket
from lib.dictionaries import Letters, DeviceSettings, Settings, WorkPlaceType

# Deafults
LOG_FILENAME = "sound_notifier.log"
LOG_LEVEL = logging.DEBUG

handler = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=4, encoding = "UTF-8")
handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(message)s"))
logger = logging.getLogger()
logger.addHandler(handler)
logger.setLevel(LOG_LEVEL)

def id_generator(size=3, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

class EnterClient():
    def __init__(self, master, server, change_server=False):
        self.reconnect = True
        self.master = master
        self.server = server
        self.change_server = change_server
        self.counter = 1
        self.subscriptions_stack = {}
        self.heartbeats_stack = []
        self.online = False
        self.wamp = None
        self.hb_worker = None

    def connect(self):
        logger.info('Try to connect...')
        try:
            self.wamp = websocket.WebSocketApp(self.server)
            self.wamp.on_message = self.on_message
            self.wamp.on_error = self.on_error
            self.wamp.on_close = self.on_close
            self.wamp.on_open = self.on_open
            self.wamp.run_forever()
        except:
            logger.warning('Warning: Server not available')
            if self.change_server and self.master.ignore_success is True:
                self.master.set_param(key='server', value=self.server)
                self.master.server_rollback = ''
                self.change_server = False

            if self.reconnect:
                time.sleep(6)
                self.connect()

    def disconnect(self, reconnect=True):
        self.online = False
        self.reconnect = reconnect
        self.wamp.close()

    def welcome(self):
        pass

    def subscribe(self, url, handler):
        self.subscriptions_stack[url] = handler
        self.wamp.send(self.encode_msg([5, url]))

    def event(self, data):
        try:
            handler = self.subscriptions_stack[data[1]]
            handler(data)
        except:
            pass

    def on_open(self, ws):
        logger.info('Connected to {}'.format(self.server))
        self.counter = 1
        self.heartbeats_stack = []
        self.subscriptions_stack = {}
        self.online = True

        if self.change_server and self.master.server_rollback_ignore_success is False:
            self.master.set_param(key='server', value=self.server)
            self.master.server_rollback = ''
            self.change_server = False

        self.hb_worker = threading.Thread(target=self.heartbeat_out)
        self.hb_worker.daemon = True
        self.hb_worker.start()

    def on_close(self, ws):
        self.online = False
        logger.warning('Connection to {} closed'.format(self.server))
        
        if self.reconnect:
            time.sleep(10)
            self.connect()

    def on_error(self, ws, error):
        logger.error('[ERROR] – {}'.format(error))

    def on_message(self, ws, message):
        data = self.decode_msg(message)
        if data[0] == 0:
            try:
                self.welcome()
            except:
                pass

        if data[0] == 8:
            logger.info('<< {}'.format(data))
            self.event(data)

        if data[0] == 20:
            self.heartbeat_in(int(data[1]))

    def heartbeat_out(self):
        while self.online:
            if len(self.heartbeats_stack) >= 5:
                self.wamp.close()
                break

            self.heartbeats_stack.append(self.counter)
            self.wamp.send(self.encode_msg([20, self.counter]))
            self.counter += 1

            time.sleep(5)

    def heartbeat_in(self, hb):
        try:
            self.heartbeats_stack.remove(hb)
        except:
            pass

    @staticmethod
    def encode_msg(message):
        return json.dumps(message)

    @staticmethod
    def decode_msg(message):
        return json.loads(message)


class Device():
    def __init__(self, settings=None):
        if settings is not None:
            self.params = Settings(settings)
        else:
            self.params = Settings(DeviceSettings)

        self.queue = deque([])
        self.app_path = os.path.dirname(os.path.realpath(__file__))
        self.media_path = self.app_path + '/media/ru-RU'
        self.tmp_path = self.app_path + '/tmp'
        self.voice_path = ['female', 'male']
        self.letters = Letters
        self._player = 'aplay'
        self.play_worker = None

    def play_notification(self, notification):
        new_file = self.generate_file(notification)

        if new_file is not None:
            self.queue.append(new_file)
            if self.play_worker is None or not self.play_worker.is_alive():
                self.play_worker = threading.Thread(target=self._play)
                self.play_worker.start()

    def generate_file(self, notification):
        chunks = []

        if self.params['EnableGong']:
            chunks.append(self.get_gong(self.params['Gong']))

        if self.params['EnableVoice']:
            chunks.append(self.get_ticket_prefix(self.params['TicketPrefix']))

            ticket_prefix = notification['TicketPrefix']
            for letter in ticket_prefix:
                chunks.append(self.get_ticket_number_prefix(letter))

            for number in self.get_ticket_numbers(notification['TicketNumber']):
                chunks.append(number)

            chunks.append(self.get_workplace_prefix(notification['WorkPlaceType']))

            for number in self.get_workplace_numbers(notification['WorkPlaceNumber']):
                chunks.append(number)

        if len(chunks):
            new_name = self.tmp_path + '/' + str(int(time.time())) + '.wav'
            fragments = []

            for chunk in chunks:
                w = wave.open(chunk, 'rb')
                fragments.append([w.getparams(), w.readframes(w.getnframes())])
                w.close()

            output = wave.open(new_name, 'wb')
            output.setparams(fragments[0][0])

            for fragment in fragments:
                output.writeframes(fragment[1])

            output.close()

            return new_name
        else:
            return None

    def _play(self):
        while len(self.queue) > 0:
            target = self.queue.popleft()
            logger.info('Play :' + target)
            try:
                subprocess.call([self._player, target])
                subprocess.call(['rm', target])
            except:
                logger.warning('Player not available')
            time.sleep(1)

    def blink(self, count=5):
        while count > 0:
            logger.info('Blink')
            try:
                subprocess.call([self._player, self.get_gong(self.params['Gong'])])
            except:
                logger.warning('Player not available')
            count -= 1

    def get_gong(self, file):
        return '{0}/Gong/{1}.wav'.format(self.media_path, file)

    def get_voice_path(self):
        return self.media_path + '/' + self.voice_path[self.params['VoiceType']]

    def get_ticket_prefix(self, prefix):
        return self.get_voice_path() + '/TicketPrefix/' + prefix.lower() + '.wav'

    def get_ticket_number_prefix(self, letter):
        return self.get_voice_path() + '/Letter/' + self.letters[letter.lower()] + '.wav'

    def get_ticket_numbers(self, number):
        numbers, files = self.split_number(number), []
        for i in range(len(numbers) - 1):
            files.append(self.get_voice_path() + "/TicketNumber/cm-" + numbers[i] + '.wav')
        files.append(self.get_voice_path() + "/TicketNumber/ce-" + numbers[-1] + '.wav')
        return files

    def get_workplace_prefix(self, workplace_type):
        key = 'PrefixFor' + WorkPlaceType[str(workplace_type)]
        prefix = self.params[key]
        return self.get_voice_path() + '/WindowPrefix/' + prefix.lower() + '.wav'

    def get_workplace_numbers(self, number):
        numbers, files = self.split_number(number), []
        for i in range(len(numbers) - 1):
            files.append(self.get_voice_path() + "/WindowNumber/wm-" + numbers[i] + '.wav')
        files.append(self.get_voice_path() + "/WindowNumber/we-" + numbers[-1] + '.wav')
        return files

    @staticmethod
    def split_number(number):
        num_string, numbers = str(number), []
        for i in range(len(num_string)):
            if int(num_string[i]) > 0:
                numbers.append(num_string[i] + '0' * (len(num_string) - 1 - i))

        if len(numbers) > 1 and int(numbers[-2]) == 10:
            new_value = int(numbers[-2]) + int(numbers[-1])
            numbers[-2:] = [str(new_value)]

        return numbers


class App():
    def __init__(self):
        self.server, self.device_id, self.client_id = "", "", ""
        self.server_rollback, self.server_rollback_ignore_success = '', False
        self.enable, self.registered = False, False
        self.Device, self.EnterClient = None, None

        self.config_file = os.path.dirname(os.path.realpath(__file__)) + '/config.ini'
        self.configurator = configparser.ConfigParser()
        self.commands_url = 'http://enter.local/clients/soundNotification#clientId={0}&deviceId={1}'
        self.events_url = 'http://enter.local/subscription/ns/notification/list'
        self.setup()

    def start(self):
        self.setup()

        self.Device = Device()

        server = self.server
        change_server = False

        if self.server_rollback != '':
            server = self.server_rollback
            change_server = True

        self.EnterClient = EnterClient(master=self, server=server, change_server=change_server)
        self.EnterClient.welcome = self.subscribe_commands
        self.EnterClient.connect()

    def setup(self):
        config = open(self.config_file, 'a')
        self.configurator.read(self.config_file)
        if 'enter' in self.configurator.sections():
            self.server = self.configurator.get('enter', 'server', fallback='')
            self.device_id = self.configurator.get('enter', 'device_id', fallback='')
            self.client_id = self.configurator.get('enter', 'client_id', fallback='')
        else:
            config.write('[enter]')
            config.close()
            self.configurator.read(self.config_file)

        if self.server == "":
            self.server = 'ws://dev.enter-systems.ru:81'
            self.set_param('server', self.server)

        if self.device_id == "":
            self.device_id = uuid.uuid4()
            self.set_param('device_id', self.device_id)

        if self.client_id == "":
            self.client_id = 'sound_' + id_generator()
            self.set_param('client_id', self.client_id)

    def subscribe_commands(self):
        self.EnterClient.subscribe(self.commands_url.format(self.client_id, self.device_id), self.get_command)

    def get_command(self, data):
        command, sound_settings = Settings(data[2]), None

        if command['IsTurnedOn'] is not None:
            self.enable = command['IsTurnedOn']

        if command['Action'] == 0:
            self.enable = bool(command['Mode'])
            return

        if command['Action'] == 1:
            self.set_param('client_id', command['Id'])
            self.EnterClient.disconnect(reconnect=False)
            self.start()
            return

        if command['Action'] == 2:
            if command['Delay']:
                time.sleep(command['Delay'])

            if command['Address']:
                self.server_rollback = command['Address']

            if command['ApplyIfServerNotAvailable'] is False:
                self.server_rollback_ignore_success = True

            self.EnterClient.disconnect(reconnect=False)
            self.start()
            return

        if command['Action'] == 3:
            blink_thread = threading.Thread(target=self.Device.blink)
            blink_thread.daemon = True
            blink_thread.start()
            return

        if command['Action'] == 4:
            self.EnterClient.disconnect()
            return

        if command['NotificationSystem'] is not None:
            self.registered = True
            sound_settings = command['NotificationSystem']['SoundDeviceSettings']
        else:
            self.registered = False

        self.Device = Device(sound_settings)

        if self.registered:
            self.get_notifications()

    def set_param(self, key, value):
        self.configurator.set('enter', str(key), str(value))
        with open(self.config_file, 'w') as configfile:
            self.configurator.write(configfile)

    def get_notifications(self):
        logger.info('Get notifications')
        self.EnterClient.subscribe(self.events_url, self.notify)

    def notify(self, data):
        notifications = Settings(data[2])
        if self.enable and self.registered:
            for item in notifications['Items']:
                self.Device.play_notification(item)


if __name__ == "__main__":
    App().start()
